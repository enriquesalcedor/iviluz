json.extract! person, :id, :identification, :name, :surname, :date_birth, :direction, :email, :phone, :users_id, :created_at, :updated_at
json.url person_url(person, format: :json)
