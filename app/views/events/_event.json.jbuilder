json.extract! event, :id, :name, :description, :date_event, :status, :created_at, :updated_at
json.url event_url(event, format: :json)
