json.extract! leadership, :id, :name, :status, :created_at, :updated_at
json.url leadership_url(leadership, format: :json)
