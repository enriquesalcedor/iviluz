json.extract! ministery, :id, :name, :description, :person_id, :created_at, :updated_at
json.url ministery_url(ministery, format: :json)
