class Leadership < ApplicationRecord
  has_many :leaders
  accepts_nested_attributes_for :leaders, allow_destroy: true

end
