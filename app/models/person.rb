class Person < ApplicationRecord
  belongs_to :users, optional: true

  mount_uploader :image, ImageUploader


  after_create :save_user


  TYPE_LEADER = [ "Jovenes", "Adultos"]
  LEVEL = [ "12", "144", "1728"]

  scope :disciples, ->{ where('id <> ?', 1)}
  scope :leader12, ->{ where('level_leader = ?', '12')}

  def codigo_aleatorio
    caracter = %w{ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 
                   a b c d e f g h i j k l m n o p q r s t u v w x y z
                   0 1 2 3 4 5 6 7 8 9 }
    posicion = rand(caracter.length)

    return caracter[posicion]
  end

  def generar_password
    aleatorio = ''
    6.times do
      aleatorio << codigo_aleatorio
    end
    password = aleatorio
    return password
  end

  def save_user
    user = User.new
    user.email = self.email
    if self.email == "admin@admin.com"
      user.password = "Admin1"
    else
      password_generated = generar_password
      user.password = password_generated
    end
    user.save
    update(users_id: user.id, password: password_generated)
  end

  def complete_name
    "#{self.name} #{self.surname}"
  end


end
