require 'test_helper'

class LeadershipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @leadership = leaderships(:one)
  end

  test "should get index" do
    get leaderships_url
    assert_response :success
  end

  test "should get new" do
    get new_leadership_url
    assert_response :success
  end

  test "should create leadership" do
    assert_difference('Leadership.count') do
      post leaderships_url, params: { leadership: { name: @leadership.name, status: @leadership.status } }
    end

    assert_redirected_to leadership_url(Leadership.last)
  end

  test "should show leadership" do
    get leadership_url(@leadership)
    assert_response :success
  end

  test "should get edit" do
    get edit_leadership_url(@leadership)
    assert_response :success
  end

  test "should update leadership" do
    patch leadership_url(@leadership), params: { leadership: { name: @leadership.name, status: @leadership.status } }
    assert_redirected_to leadership_url(@leadership)
  end

  test "should destroy leadership" do
    assert_difference('Leadership.count', -1) do
      delete leadership_url(@leadership)
    end

    assert_redirected_to leaderships_url
  end
end
