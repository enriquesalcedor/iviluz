require "application_system_test_case"

class LeadershipsTest < ApplicationSystemTestCase
  setup do
    @leadership = leaderships(:one)
  end

  test "visiting the index" do
    visit leaderships_url
    assert_selector "h1", text: "Leaderships"
  end

  test "creating a Leadership" do
    visit leaderships_url
    click_on "New Leadership"

    fill_in "Name", with: @leadership.name
    fill_in "Status", with: @leadership.status
    click_on "Create Leadership"

    assert_text "Leadership was successfully created"
    click_on "Back"
  end

  test "updating a Leadership" do
    visit leaderships_url
    click_on "Edit", match: :first

    fill_in "Name", with: @leadership.name
    fill_in "Status", with: @leadership.status
    click_on "Update Leadership"

    assert_text "Leadership was successfully updated"
    click_on "Back"
  end

  test "destroying a Leadership" do
    visit leaderships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Leadership was successfully destroyed"
  end
end
