require "application_system_test_case"

class MinisteriesTest < ApplicationSystemTestCase
  setup do
    @ministery = ministeries(:one)
  end

  test "visiting the index" do
    visit ministeries_url
    assert_selector "h1", text: "Ministeries"
  end

  test "creating a Ministery" do
    visit ministeries_url
    click_on "New Ministery"

    fill_in "Description", with: @ministery.description
    fill_in "Name", with: @ministery.name
    fill_in "Person", with: @ministery.person_id
    click_on "Create Ministery"

    assert_text "Ministery was successfully created"
    click_on "Back"
  end

  test "updating a Ministery" do
    visit ministeries_url
    click_on "Edit", match: :first

    fill_in "Description", with: @ministery.description
    fill_in "Name", with: @ministery.name
    fill_in "Person", with: @ministery.person_id
    click_on "Update Ministery"

    assert_text "Ministery was successfully updated"
    click_on "Back"
  end

  test "destroying a Ministery" do
    visit ministeries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ministery was successfully destroyed"
  end
end
