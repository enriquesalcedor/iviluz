# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_13_005444) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.date "date_event"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leaders", force: :cascade do |t|
    t.bigint "leadership_id"
    t.integer "person"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["leadership_id"], name: "index_leaders_on_leadership_id"
  end

  create_table "leaderships", force: :cascade do |t|
    t.string "name"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ministeries", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.bigint "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_ministeries_on_person_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "identification"
    t.string "name"
    t.string "surname"
    t.date "date_birth"
    t.text "direction"
    t.string "email"
    t.string "phone"
    t.integer "leader"
    t.string "type_leader"
    t.string "level_leader"
    t.string "password"
    t.string "type_user"
    t.boolean "is_admin"
    t.bigint "users_id"
    t.integer "user_created"
    t.integer "user_updated"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.index ["users_id"], name: "index_people_on_users_id"
  end

  create_table "seed_migration_data_migrations", id: :serial, force: :cascade do |t|
    t.string "version"
    t.integer "runtime"
    t.datetime "migrated_on"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "leaders", "leaderships"
  add_foreign_key "ministeries", "people"
  add_foreign_key "people", "users", column: "users_id"
end
