class CreateLeaders < ActiveRecord::Migration[5.2]
  def change
    create_table :leaders do |t|
      t.references :leadership, foreign_key: true
      t.integer :person

      t.timestamps
    end
  end
end
