class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :identification
      t.string :name
      t.string :surname
      t.date :date_birth
      t.text :direction
      t.string :email
      t.string :phone
      t.integer :leader
      t.string :type_leader
      t.string :level_leader
      t.string :password
      t.string :type_user
      t.boolean :is_admin

      t.references :users, foreign_key: true

      t.integer :user_created
      t.integer :user_updated
      t.timestamps

    end
  end
end
