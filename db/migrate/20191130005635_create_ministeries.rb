class CreateMinisteries < ActiveRecord::Migration[5.2]
  def change
    create_table :ministeries do |t|
      t.string :name
      t.text :description
      t.references :person, foreign_key: true

      t.timestamps
    end
  end
end
