class AddUserAdmin < SeedMigration::Migration
  def up
    [
      {
        name: "User",
        surname:"Admin",
        email: "admin@admin.com",
        password: "Admin1",
        type_user: "Admin",
        is_admin: true
      }

    ].each do |person|
      Person.create(person)
    end
  end

  def down
    Person.destroy.all
  end
end