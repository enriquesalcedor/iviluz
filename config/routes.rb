Rails.application.routes.draw do
  resources :leaderships
  resources :ministeries
  	get 'ministeries/:id/team' , to: 'ministeries#team'
  	
  resources :events
  resources :people
  devise_for :users
  root to: "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
